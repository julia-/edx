# EdX

## Table of Contents
  - [Getting started](#getting-started)
    - [Prerequisites](#prerequisites)
    - [Installation](#installation)
  - [About](#about)
    - [Problem](#problem)
      - [Part 1](#part-1)
      - [Part 2](#part-2)
      - [Part 3](#part-3)
      - [Part 4 (Optional)](#part-4-optional)
    - [Solution](#solution)
  - [Challenges and final thoughts](#challenges-and-final-thoughts)
  
## Getting started
These instructions will get you a copy of the project up and running on your local machine for development purposes.

### Prerequisites
- Create React App => v1.1.5
- Yarn => v1.9.4
- Node.js => v10.9.0
- npm => v6.2.0

#### Environments
This application was developed on macOS Sierra Version 10.12.6. You will need to update the NODE_PATH in the package.json for absolute imports to work if using Windows.

### Installation
Clone the repo:
```
git clone https://gitlab.com/julia-/edx.git
```

Install application dependencies and devDependencies:
```
cd ed-x
yarn install
```

Update the NODE_PATHS if using Windows:
```
yarn add cross-env --dev
```

Update the scripts in the package.json (Windows only):
```
"start": "cross-env NODE_PATH=src:src/components:src/containers:src/context react-scripts start",
"build": "cross-env NODE_PATH=src:src/components:src/containers:src/context react-scripts build",
"test": "cross-env NODE_PATH=src react-scripts test --env=jsdom",
```

Start the app in development mode:
```
yarn start
```

If the application doesn't automatically open up in your browser, open http://localhost:3000 in the browser.

Run the tests:
```
yarn test
```

Build the app for production:
```
yarn build
```

## About
### Problem

Company X is developing a new app for student education. Students complete quizzes and their progress is recorded. Each quiz has 2-4 questions. There are 5 quizzes in total.

#### Part 1

Write a web app that displays the quizzes provided (lessons.json, quizzes.json).
It should have a page to view an individual quiz.

#### Part 2

Allow a student to answer questions in the quizzes and submit an entire quiz once all questions have been answered.

#### Part 3

Students start from Quiz 1 and progress through to Quiz 5. They should only be allowed to access the next quiz (e.g. quiz 2) when they have finished the prior quiz (e.g. quiz 1).

#### Part 4 (Optional)

Add a page to show the student's progress through all the quizzes and their scores for each quiz they have completed.

### Solution
Build a web application to display quizzes and results using React.

## Challenges and final thoughts

It was challengening to complete Part 4 due to the data structure for the user answers. Part 4 continues to need further work as all the user's answers for a quiz display under each question in the the quiz.

Managing state using the React Context API helped sharing data between components and nested components. Without using the Context API or a state management tool it would have been much more difficulty.

## Todo
[] Unit testing with snapshots
[] Stying the CompletePage container
[] Fix issue with all user answers under each question in a quiz in the CompletePage container
[] Add quiz progress information to the CompletePage container
