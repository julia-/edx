import React from 'react';
import quizzes from 'data/quizzes.json';
import questions from 'data/questions.json';

export const DEFAULT_STATE = {
  checkedAnswers: [],
  currentQuestions: [],
  questionData: questions,
  quizData: quizzes,
  quizId: 1,
  isComplete: false,
  isDisabled: false,
  percentage: 0,
  selectedAnswers: {},
};

export const AppContext = React.createContext(DEFAULT_STATE);
