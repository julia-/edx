import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { DEFAULT_STATE, AppContext } from 'App.context';

class Provider extends Component {
  state = DEFAULT_STATE;

  static propTypes = {
    children: PropTypes.node.isRequired,
  };

  updateQuizId = quizId => {
    const {
      state: { isComplete, isDisabled },
    } = this;

    this.setState({
      isComplete: !isComplete,
      isDisabled: !isDisabled,
      quizId: quizId + 1,
      selectedAnswers: [],
    });
  };

  getQuiz = quizId => {
    const {
      state: {
        quizData: { quizzes },
      },
    } = this;

    return quizzes.filter(quiz => quiz.id === quizId)[0];
  };

  getQuestions = questionIds => {
    const {
      state: {
        questionData: { questions },
      },
    } = this;

    const currentQuestions = questions.filter(({ id }) => questionIds.includes(id));
    this.setState({ currentQuestions });
  };

  getPercentage = quizId => {
    const {
      state: {
        quizData: { quizzes },
      },
    } = this;

    const percentage = (quizId / quizzes.length) * 100;
    this.setState({ percentage });
  };

  handleSelectedAnswer = (questionId, e) => {
    const { selectedAnswers } = this.state;
    const currentSelectedAnswer = {
      [questionId]: e.target.value,
    };
    const newSelectedAnswers = {
      ...selectedAnswers,
      ...currentSelectedAnswer,
    };
    this.setState({ selectedAnswers: newSelectedAnswers });
  };

  handleQuizSubmission = () => {
    const {
      state: { quizId, selectedAnswers, currentQuestions, isDisabled, isComplete, checkedAnswers },
    } = this;

    if (isDisabled) return;

    const currentAnswers = {};

    currentQuestions.forEach(question => {
      const { id } = question;

      Object.entries(selectedAnswers).forEach(([key, value]) => {
        const userAnswer = parseInt(value.split('-')[1], 10);
        const isCorrect = question.correctAnswer === userAnswer;
        const answerKey = parseInt(key, 10);

        if (answerKey === id) {
          if (!currentAnswers[quizId]) {
            currentAnswers[quizId] = [];
          }
          currentAnswers[quizId].push({
            [id]: { isCorrect, userAnswer },
          });
        }
        return currentAnswers;
      });
      return currentAnswers;
    });

    this.getPercentage(quizId);

    this.setState({
      checkedAnswers: [...checkedAnswers, currentAnswers],
      isDisabled: !isDisabled,
      isComplete: !isComplete,
    });
  };

  render() {
    const { children } = this.props;

    return (
      <AppContext.Provider
        value={{
          ...this.state,
          handleQuizSubmission: this.handleQuizSubmission,
          handleSelectedAnswer: this.handleSelectedAnswer,
          getPercentage: this.getPercentage,
          getQuestions: this.getQuestions,
          getQuiz: this.getQuiz,
          updateQuizId: this.updateQuizId,
        }}
      >
        {children}
      </AppContext.Provider>
    );
  }
}

export default Provider;
