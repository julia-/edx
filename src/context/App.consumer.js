/* eslint react/prefer-stateless-function: "off" */

import React from 'react';
import { AppContext } from 'App.context';

const withContext = Component =>
  class WithContext extends React.Component {
    render() {
      return (
        <AppContext.Consumer>
          {context => <Component {...this.props} context={context} />}
        </AppContext.Consumer>
      );
    }
  };

export default withContext;
