import React from 'react';
import PropTypes from 'prop-types';
import Result from 'Result/Result';
import withContext from 'App.consumer';
import './ResultsList.css';

const ResultWithContext = withContext(({ context, ...rest }) => (
  <Result context={context} {...rest} />
));

const ResultsList = ({ context: { checkedAnswers } }) => (
  <section className="ResultsList">
    {checkedAnswers &&
      checkedAnswers.map(result => (
        <section key={Object.keys(result)} className="ResultsList__container">
          <h3 className="ResultsList__heading">{`Quiz ${Object.keys(result)} Results`}</h3>
          <ResultWithContext
            key={Object.keys(result)}
            resultDetails={result}
            questionId={Object.keys(result)}
            quizId={Object.keys(result)}
          />
        </section>
      ))}
  </section>
);

ResultsList.propTypes = {
  context: PropTypes.shape({
    checkedAnswers: PropTypes.arrayOf(PropTypes.object).isRequired,
  }).isRequired,
};

export default ResultsList;
