import React from 'react';
import PropTypes from 'prop-types';

const ProgressFill = ({ percentage }) => (
  <div className="ProgressFill" style={{ width: `${percentage}%` }} />
);

ProgressFill.propTypes = {
  percentage: PropTypes.number.isRequired,
};

export default ProgressFill;
