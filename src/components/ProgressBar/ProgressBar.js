import React from 'react';
import PropTypes from 'prop-types';
import ProgressFill from './ProgressFill';
import './ProgressBar.css';

const ProgressBar = ({ context: { percentage } }) => (
  <section className="ProgressBar">
    <h3 className="ProgressBar__heading">Your progress</h3>
    <div className="ProgressBar__outline">
      <ProgressFill percentage={percentage} />
    </div>
  </section>
);

ProgressBar.propTypes = {
  context: PropTypes.shape({
    percentage: PropTypes.number.isRequired,
  }).isRequired,
};

export default ProgressBar;
