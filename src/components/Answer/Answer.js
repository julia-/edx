import React from 'react';
import PropTypes from 'prop-types';
import './Answer.css';

const Answer = ({
  index,
  questionId,
  answer,
  context: { handleSelectedAnswer, quizId, isDisabled },
}) => (
  <label
    key={`${quizId}_${questionId}-${index}`}
    htmlFor={`${quizId}_${questionId}-${index}`}
    className="Answer__label"
  >
    <input
      type="radio"
      id={`${quizId}_${questionId}-${index}`}
      name={questionId}
      value={`${quizId}_${questionId}-${index}`}
      className="Answer__input"
      disabled={isDisabled}
      onChange={e => handleSelectedAnswer(questionId, e)}
    />
    {answer}
  </label>
);

Answer.propTypes = {
  index: PropTypes.number.isRequired,
  questionId: PropTypes.number.isRequired,
  answer: PropTypes.string.isRequired,
  context: PropTypes.shape({
    handleSelectedAnswer: PropTypes.func.isRequired,
    quizId: PropTypes.number.isRequired,
    isDisabled: PropTypes.bool.isRequired,
  }).isRequired,
};

export default Answer;
