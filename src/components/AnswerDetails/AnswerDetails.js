import React, { Fragment } from 'react';
import PropTypes from 'prop-types';
import './AnswerDetails.css';

const AnswerDetails = props => {
  const {
    question: { answers, correctAnswer },
    result: { isCorrect, userAnswer },
  } = props;

  return (
    <Fragment>
      <p className="AnswerDetails__result">
        {`You answered ${answers[userAnswer]}. This is 
              ${isCorrect ? 'correct' : 'incorrect'}. 
              ${!isCorrect ? `The correct answer is ${answers[correctAnswer]}.` : ''}`}
      </p>
    </Fragment>
  );
};

AnswerDetails.propTypes = {
  question: PropTypes.shape({
    answers: PropTypes.array,
    correctAnswer: PropTypes.number,
  }).isRequired,
  result: PropTypes.shape({
    isCorrect: PropTypes.object,
    userAnswer: PropTypes.object,
  }).isRequired,
};

export default AnswerDetails;
