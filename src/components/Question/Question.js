/* eslint react/no-array-index-key: "off" */

import React from 'react';
import PropTypes from 'prop-types';
import Answer from 'Answer/Answer';
import withContext from 'App.consumer';
import './Question.css';

const AnswerWithContext = withContext(({ context, ...rest }) => (
  <Answer context={context} {...rest} />
));

const Question = ({ question }) => (
  <section key={question.id}>
    <h3 className="Question__heading">{`Question ${question.id}`}</h3>
    <h4 className="Question__heading--question">{question.question}</h4>
    <fieldset className="Question__fieldset">
      <legend className="Question__legend">Answers</legend>
      {question.answers.map((answer, index) => (
        <AnswerWithContext key={index} questionId={question.id} answer={answer} index={index} />
      ))}
    </fieldset>
  </section>
);

Question.propTypes = {
  question: PropTypes.shape({
    id: PropTypes.number.isRequired,
    question: PropTypes.string.isRequired,
    answers: PropTypes.arrayOf(PropTypes.string).isRequired,
    correctAnswer: PropTypes.number.isRequired,
  }).isRequired,
};

export default Question;
