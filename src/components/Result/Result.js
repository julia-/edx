import React, { Fragment } from 'react';
import PropTypes from 'prop-types';
import AnswerDetails from 'AnswerDetails/AnswerDetails';
import './Result.css';

const Result = props => {
  const {
    context: {
      questionData: { questions },
      quizData: { quizzes },
    },
    questionId,
    quizId,
    resultDetails,
  } = props;

  const getQuiz = quizzes.filter(({ id }) => id === parseInt(quizId, 10))[0];

  const currentQuestions = questions.filter(({ id }) => getQuiz.questionIds.includes(id));

  const resultValues = Object.values(resultDetails);

  return (
    <section>
      {currentQuestions.map(question => (
        <Fragment key={questionId}>
          <h4 className="Result__heading">{`Question ${question.id}: ${question.question}`}</h4>
          {resultValues[0].map(
            result =>
              Number(Object.keys(result)[0]) === question.id && (
                <AnswerDetails key={question.id} question={question} result={result[question.id]} />
              )
          )}
        </Fragment>
      ))}
    </section>
  );
};

Result.propTypes = {
  context: PropTypes.shape({
    questionData: PropTypes.objectOf(PropTypes.array).isRequired,
    quizData: PropTypes.objectOf(PropTypes.array).isRequired,
  }).isRequired,
  questionId: PropTypes.arrayOf(PropTypes.string).isRequired,
  quizId: PropTypes.arrayOf(PropTypes.string).isRequired,
  resultDetails: PropTypes.objectOf(PropTypes.array).isRequired,
};

export default Result;
