import React from 'react';
import PropTypes from 'prop-types';
import './NavButton.css';

const NavButton = ({ context: { updateQuizId, quizId } }) => (
  <button type="submit" onClick={() => updateQuizId(quizId)} className="NavButton--next">
    Start next quiz
  </button>
);

NavButton.propTypes = {
  context: PropTypes.shape({
    updateQuizId: PropTypes.func.isRequired,
  }).isRequired,
};

export default NavButton;
