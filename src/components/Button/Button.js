import React from 'react';
import PropTypes from 'prop-types';
import './Button.css';

const Button = ({
  context: { handleQuizSubmission, selectedAnswers, currentQuestions, isDisabled },
}) => (
  <button
    type="submit"
    disabled={Object.keys(selectedAnswers).length !== currentQuestions.length || isDisabled}
    onClick={e => handleQuizSubmission(e)}
    className="Button--submit"
  >
    Submit
  </button>
);

Button.propTypes = {
  context: PropTypes.shape({
    handleQuizSubmission: PropTypes.func.isRequired,
  }).isRequired,
};

export default Button;
