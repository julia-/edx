/* eslint react/prefer-stateless-function: "off", no-unused-vars: "off" */
import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import ResultsList from 'ResultsList/ResultsList';
import ProgressBar from 'ProgressBar/ProgressBar';
import { AppContext } from 'App.context';
import withContext from 'App.consumer';
import './CompletePage.css';

const ResultsListWithContext = withContext(({ context, ...rest }) => (
  <ResultsList context={context} {...rest} />
));

const ProgressBarWithContext = withContext(({ context, ...rest }) => (
  <ProgressBar context={context} {...rest} />
));

class CompletePage extends Component {
  render() {
    return (
      <AppContext.Consumer>
        {context => (
          <section className="CompletePage">
            <h2 className="CompletePage__heading">Quiz Results</h2>
            <ProgressBarWithContext />
            <ResultsListWithContext />
            <Link to="/" className="CompletePage__link">
              Back to quiz
            </Link>
          </section>
        )}
      </AppContext.Consumer>
    );
  }
}

export default CompletePage;
