/* eslint react/prefer-stateless-function: "off" */

import React, { Component } from 'react';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import Provider from 'App.provider';
import QuizPage from 'QuizPage/QuizPage';
import CompletePage from 'CompletePage/CompletePage';
import withContext from 'App.consumer';
import './App.css';

const QuizWithContext = withContext(({ context }) => <QuizPage context={context} />);

class App extends Component {
  render() {
    return (
      <Router>
        <Provider>
          <main className="App">
            <h1>Ed-X</h1>
            <Switch>
              <Route path="/complete" component={CompletePage} />
              <Route path="/" component={QuizWithContext} />
            </Switch>
          </main>
        </Provider>
      </Router>
    );
  }
}

export default App;
