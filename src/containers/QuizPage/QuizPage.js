/* eslint react/prefer-stateless-function: "off", react/no-array-index-key: "off", react/sort-comp: "off" */

import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import Button from 'Button/Button';
import NavButton from 'NavButton/NavButton';
import ProgressBar from 'ProgressBar/ProgressBar';
import Question from 'Question/Question';
import withContext from 'App.consumer';
import './QuizPage.css';

const ButtonWithContext = withContext(({ context, ...rest }) => (
  <Button context={context} {...rest} />
));

const NavButtonWithContext = withContext(({ context, ...rest }) => (
  <NavButton context={context} {...rest} />
));

const ProgressBarWithContext = withContext(({ context, ...rest }) => (
  <ProgressBar context={context} {...rest} />
));

class QuizPage extends Component {
  static propTypes = {
    context: PropTypes.shape({
      quizId: PropTypes.number.isRequired,
      quizData: PropTypes.objectOf(PropTypes.array).isRequired,
      questionData: PropTypes.objectOf(PropTypes.array).isRequired,
    }).isRequired,
  };

  load() {
    const {
      context: { quizId, getQuiz, getQuestions, getPercentage },
    } = this.props;

    const currentQuizData = getQuiz(quizId);
    const { questionIds } = currentQuizData;
    getPercentage(quizId);
    getQuestions(questionIds);
  }

  componentDidMount() {
    this.load();
  }

  componentDidUpdate(prevProps) {
    const {
      context: { quizId },
    } = this.props;
    // render quiz when quizId changes
    if (quizId !== prevProps.context.quizId) {
      this.load();
    }
  }

  render() {
    const {
      context: {
        quizId,
        currentQuestions,
        isComplete,
        quizData: { quizzes },
      },
    } = this.props;

    return (
      <section>
        <h2>{`Quiz ${quizId}`}</h2>
        {currentQuestions &&
          currentQuestions.map(question => <Question key={question.id} question={question} />)}
        <ButtonWithContext />
        {/* Display button for next quiz after quiz submitted. Hide button if all quizzes have been completed. */}
        {isComplete && quizId !== quizzes.length && <NavButtonWithContext />}
        <ProgressBarWithContext />
        <Link to="/complete" className="Quiz__link">
          View results
        </Link>
      </section>
    );
  }
}

export default QuizPage;
